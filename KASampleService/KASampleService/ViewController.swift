//
//  ViewController.swift
//  KASampleService
//
//  Created by Kasun Aravinda Karunarathna on 11/25/16.
//  Copyright © 2016 NTB. All rights reserved.
//

import UIKit
import MBProgressHUD
import NVActivityIndicatorView

class ViewController: UIViewController, NVActivityIndicatorViewable {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dataManager = KADataManager.sharedDataInstance
        
        let helper = KAHelper.sharedInstance  // helper instance
    
        
        // Start Activity Indicator
        helper.showLoadingProgressHUDOnWindow()
        
       
        dataManager.getAllComments{ (data, error) in
            
           helper.hideHudOnWindow()
            
            if (error != nil) {
                print(error)
                
            }
            else{
            
                print(data)
                
            }
            
        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

}

