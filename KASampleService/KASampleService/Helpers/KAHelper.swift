//
//  KAHelper.swift
//  KASampleService
//
//  Created by Kasun Aravinda Karunarathna on 11/25/16.
//  Copyright © 2016 . All rights reserved.
//

import UIKit
import Foundation
import MBProgressHUD

class KAHelper: NSObject
{
//    private static var __once: () = {
//            Static.instance = KAHelper()
//        }()
//    class var sharedInstance: KAHelper
//    {
//        struct Static
//        {
//            static var onceToken: Int = 0
//            static var instance: KAHelper? = nil
//        }
//        _ = KAHelper.__once
//        return Static.instance!
//    }
    
    static let sharedInstance = KAHelper()
    
    // MARK: - MBProgressHud
    
    func showProgressHUD(_ message:String ,load_on_view :UIView)
    {
        let hud = MBProgressHUD.showAdded(to: load_on_view, animated: true)
        hud.label.text = message;
    }
    
    func showProgressHUDOnWindow(_ message:String)
    {
        DispatchQueue.main.async {
            
            let hud = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!, animated: true)
            hud.label.text = message
            
        }
    }
    
    
    func showLoadingProgressHUD(_ message: String, view:UIView)
    {
        showProgressHUD(message, load_on_view: view)
    }
    
    
    func showLoadingProgressHUDOnWindow()
    {
        showProgressHUDOnWindow("Loading")
    }
    
    func hideHud(_ load_on_view :UIView)
    {
        MBProgressHUD.hide(for: load_on_view, animated: true)
    }
    func hideHudOnWindow ()
    {
        DispatchQueue.main.async
        {
            MBProgressHUD.hide(for: UIApplication.shared.keyWindow!, animated: true)
        }
    }
    
    internal func showToast(_ message:String , load_on_view :UIView)
    {
        let hud = MBProgressHUD.showAdded(to: load_on_view, animated: true)
        // Configure for text only and offset down
        hud.mode = MBProgressHUDMode.text
        hud.detailsLabel.text = message
        hud.detailsLabel.font = UIFont (name: "Helvetica", size: 16.0)
        hud.margin = 10.0
        hud.margin = 10.0
        
        if UIDevice.current.orientation.isPortrait
        {
            hud.offset.y = 200.0
        }
        else
        {
            hud.offset.x = 120.0
        }
        
        
        hud.removeFromSuperViewOnHide = true
        hud.hide(animated: true, afterDelay: 3)
    }
    
    func showToastOnWindow(_ message:String , load_on_view :UIView)
    {
        
        if ((load_on_view.window) == nil)
        {
            return;
        }
        
        let hud = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!, animated: true)
        // Configure for text only and offset down
        hud.mode = MBProgressHUDMode.text
        hud.detailsLabel.text = message
        hud.detailsLabel.font = UIFont (name: "Helvetica", size: 16.0)
        hud.margin = 10.0
        hud.margin = 10.0
        
        if UIDevice.current.orientation.isPortrait
        {
            hud.offset.x = 200.0
        }
        else
        {
            hud.offset.y = 120.0
        }
        
        hud.removeFromSuperViewOnHide = true
        hud.hide(animated: true, afterDelay: 3)
        
    }
    
}
