//
//  KAServiceManager.swift
//  KASampleService
//
//  Created by Kasun Aravinda Karunarathna on 11/25/16.
//  Copyright © 2016 . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class KAServiceManager: NSObject
{
//    private static var __once: () = {
//           // Static.instance = KAServiceManager()
//        }()
    var alamofireManager : Alamofire.SessionManager!
    var configuration : URLSessionConfiguration!
    let reachabilityManager = NetworkReachabilityManager()
    
    /*
     For adding http headers
     */
   // let headers : HTTPHeaders!


     static let sharedServiceInstance = KAServiceManager()
    
    
    override init ()
    {
        configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 30 // seconds
        self.alamofireManager = Alamofire.SessionManager(configuration: configuration)
        
        // Ex -
        // headers = ["Authorization": "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==",
        //           "Content-Type": "application/x-www-form-urlencoded"]
        
    }
    
    //    Generic GET
    func common_get(url_string : String, parameters : [String: AnyObject]?, success:@escaping ((AnyObject?)->(Void)), falier:@escaping ((AnyObject?)->(Void)))
    {
     
        
        self.alamofireManager.request(url_string, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: requestHeaders).responseJSON { (response) in
            
            switch response.result
            {
            case .success(let json):
                print("Success with JSON: \(json)")
                success(json as AnyObject?)
            case .failure(let error):
                //print("Success with JSON: \(error)")
                falier(error as AnyObject?)
                
            }
        }
    }
    
    //    Generic POST
    func common_post(url_string : String, parameters : [String: AnyObject]?, success:@escaping ((AnyObject?)->(Void)), falier:@escaping ((AnyObject?)->(Void)))
    {
        self.alamofireManager.request(url_string, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: requestHeaders).responseJSON { (response) in
            switch response.result
            {
            case .success(let json):
                print("Success with JSON: \(json)")
                
                success(json as AnyObject?)
                
            case .failure(let error):
                
                falier(error as AnyObject?)
            }
        }
        
    }
    
    //    Generic PUT
    func common_put(url_string : String, parameters : [String: AnyObject]?, success:@escaping ((AnyObject?)->()), falier:@escaping ((AnyObject?)->(Void)))
    {
        self.alamofireManager.request(url_string, method: .put,  parameters: parameters, encoding: JSONEncoding.default, headers: requestHeaders).responseJSON { (response) in
            switch response.result
            {
            case .success(let json):
                print("Success with JSON: \(json)")
                
                success(json as AnyObject?)
                
            case .failure(let error):
                
                falier(error as AnyObject?)
            }
        }
    }
    
    //    Generic DELETE
    func common_delete(url_string : String, parameters : [String: AnyObject?], success:@escaping ((AnyObject?)->(Void)), falier:@escaping ((AnyObject?)->(Void)))
    {
        self.alamofireManager.request(url_string, method:.delete, parameters: parameters, encoding: JSONEncoding.default, headers: requestHeaders).responseJSON { (response) in
            switch response.result
            {
                
            case .success(let json):
                print("Success with JSON: \(json)")
                
                success(json as AnyObject?)
                
            case .failure(let error):
                
                falier(error as AnyObject?)
            }
        }
    }

    /*
     https://github.com/Alamofire/Alamofire
     https://grokswift.com/rest-with-alamofire-swiftyjson/
     http://stackoverflow.com/questions/32580412/handle-no-internet-connection-error-before-try-to-parse-the-result-in-alamofire
     http://stackoverflow.com/questions/35427698/how-to-use-networkreachabilitymanager-in-alamofire
     */
}
