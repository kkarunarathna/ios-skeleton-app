//
//  KADataManager.swift
//  KASampleService
//
//  Created by Kasun Aravinda Karunarathna on 11/25/16.
//  Copyright © 2016 . All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class KADataManager: NSObject
{
    
    static let sharedDataInstance = KADataManager()
    
    var serviceManager = KAServiceManager.sharedServiceInstance

    
    
    // Get location list
    func getNearByLocations( completionHandler:@escaping ([KALocation]?, NSError?) -> Void) {
    
        serviceManager.common_get(url_string: base_url, parameters: nil, success: { (data) in
            
        
            let obj_response = data as! NSArray
            
            var arr_locations = [KALocation]()
            
            if(obj_response.count != 0)
            {
                for locations in obj_response
                {
                    let json = JSON(locations)
                    let location = KALocation(json: json)
                    
                    arr_locations.append(location)
                }
            }
    
            completionHandler(arr_locations, nil)
                                                    
        }) { (error) in
            
            completionHandler(nil, error as? NSError)
            
        }
    }
    
    func getAllComments(completionHandler:@escaping ([KAComment]?, NSError?) -> Void) {
        
        serviceManager.common_get(url_string: base_url, parameters: nil, success: { (data) in
            
            
            let obj_response = data as! NSArray
            
            var arr_locations = [KAComment]()
            
            if(obj_response.count != 0)
            {
                for locations in obj_response
                {
                    let json = JSON(locations)
                    let location = KAComment(json: json)
                    
                    arr_locations.append(location)
                }
            }
            
            completionHandler(arr_locations, nil)
            
        }) { (error) in
            
            completionHandler(nil, error as? NSError)
            
        }
    }

}
