//
//  KAComment.swift
//  KASampleService
//
//  Created by Kasun Aravinda on 12/20/17.
//  Copyright © 2017 NTB. All rights reserved.
//

import Foundation
import SwiftyJSON



class KAComment: NSObject
    //, NSCoding
{
    internal var user_id : Int = 0
    internal var comment_id : Int = 0
    internal var comment_title : String?
    internal var comment_body : String?
    
    override init() {}
    
    init(json: SwiftyJSON.JSON) {
        
        user_id = json["userId"].intValue
        comment_id = json["id"].intValue
        comment_title = json["title"].stringValue
        comment_body = json["body"].stringValue
        
    }
    
    
    required init(coder aDecoder: NSCoder)
    {
        self.user_id = aDecoder.decodeInteger(forKey: "user_id")
        self.comment_id = aDecoder.decodeInteger(forKey: "comment_id")
        
        if let comment_title = aDecoder.decodeObject(forKey: "comment_title") as? String
        {
            self.comment_title = comment_title
        }
        
        if let comment_body = aDecoder.decodeObject(forKey: "comment_body") as? String
        {
            self.comment_body = comment_body
        }
    }
    
    func encodeWithCoder(aCoder: NSCoder)
    {
        aCoder.encode(user_id, forKey: "user_id")
        aCoder.encode(comment_id, forKey: "comment_id")
        
        if let comment_title = self.comment_title
        {
            aCoder.encode(comment_title, forKey: "comment_title")
        }
        
        if let comment_body = self.comment_body
        {
            aCoder.encode(comment_body, forKey: "comment_body")
        }
        
    }
    
    
    
    
    
    
}
