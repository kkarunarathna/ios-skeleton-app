//
//  KALocation.swift
//  KASampleService
//
//  Created by Kasun Aravinda Karunarathna on 3/14/17.
//  Copyright © 2017 NTB. All rights reserved.
//

import Foundation
import SwiftyJSON



class KALocation: NSObject
    //, NSCoding
{
    internal var location_id : Int = 0
    internal var location_name : String?
    internal var location_type : String?
    internal var location_sub_type : String = ""
    internal var location_latitude : Double = 0.0
    internal var location_longitude : Double = 0.0
    internal var location_isDeleted : Bool = false
    internal var location_keywords : String = ""
    internal var location_priority : Int = 0
    
    override init() {}
    
    init(json: SwiftyJSON.JSON) {
        
        location_id = json["LocationId"].intValue
        location_name = json["LocationName"].stringValue
        location_type = json["LocationType"].stringValue
        location_sub_type = json["SubType"].stringValue
        location_latitude = json["Latitude"].doubleValue
        location_longitude = json["Longitude"].doubleValue
        location_isDeleted = json["IsDeleted"].boolValue
        location_keywords = json["Keywords"].stringValue
        location_priority = json["Priority"].intValue
    }
    
    
    required init(coder aDecoder: NSCoder)
    {
        self.location_id = aDecoder.decodeInteger(forKey: "location_id")
        
        if let location_name = aDecoder.decodeObject(forKey: "location_name") as? String
        {
            self.location_name = location_name
        }
        
        if let location_type = aDecoder.decodeObject(forKey: "location_type") as? String
        {
            self.location_type = location_type
        }
        
        self.location_latitude = aDecoder.decodeDouble(forKey: "location_latitude")
        
        self.location_longitude = aDecoder.decodeDouble(forKey: "location_longitude")
        
    }
    
    func encodeWithCoder(aCoder: NSCoder)
    {
        aCoder.encode(location_id, forKey: "location_id")
        
        if let location_name = self.location_name
        {
            aCoder.encode(location_name, forKey: "location_name")
        }
        
        if let location_type = self.location_type
        {
            aCoder.encode(location_type, forKey: "location_type")
        }
        
        aCoder.encode(location_latitude, forKey: "location_latitude")
        
        aCoder.encode(location_longitude, forKey: "location_longitude")
        
        
    }
    
    
    
    
    
    
}
